__author__ = 'bmiller'

import os
import sys
import shutil
from ctypes import CDLL

from pydot import dot_parser
from gui_ui import Ui_Dialog
from PyQt4.QtGui import QApplication, QDialog, QFileDialog

from pe_lib import pe_library

from netgen import NetGen
from target import Target
from dot2grid import initial_placement


class GUI(QDialog, Ui_Dialog):
    def __init__(self):
        super(GUI, self).__init__()
        self.setupUi(self)

        self._populate_targets()

        self.generate_vhdl_button.clicked.connect(self.generate_vhdl)
        self.generate_constrained_vhdl_button.clicked.connect(self.generate_constrained_vhdl)
        self.generate_dot_button.clicked.connect(self.generate_dot)
        self.target_combobox.currentIndexChanged.connect(self.target_changed)

    def _populate_targets(self):
        self.target_combobox.addItems([f for f in os.listdir('targets') if not f.endswith('.vhd')])
        self.target_changed()

    def target_changed(self):
        current_target = str(self.target_combobox.currentText())
        grids_dir = os.path.join('targets', current_target, 'grids')
        self.grid_combobox.clear()
        self.grid_combobox.addItems([f for f in os.listdir(grids_dir) if f.endswith('.config')])

    def _gather_fields(self):
        self.topology = str(self.topology_combobox.currentText())
        self.target = str(self.target_combobox.currentText())
        self.grid = str(self.grid_combobox.currentText())
        self.bus_width = int(self.buswidth_combobox.currentText())
        self.num_pes = int(self.pe_textedit.text())
        self.dsp_per_pe = int(self.dsp_per_pe_lineedit.text())
        self.ram_per_pe = int(self.ram_per_pe_lineedit.text())
        self.avg_degree = int(self.mean_lineedit.text())
        self.std_dev = int(self.std_dev_lineedit.text())
        self.mesh_height = int(self.mesh_height_lineedit.text())
        self.mesh_width = int(self.mesh_width_lineedit.text())

    def _set_fields(self):
        if self.topology == 'Mesh':
            self.num_pes = self.mesh_height * self.mesh_width
        self.dsp_textedit.setText(str(self.num_pes * self.dsp_per_pe))
        self.bram_textedit.setText(str(self.num_pes * self.ram_per_pe))
        self.luts_textedit.setText(str(250 * self.num_pes))

    def _build_graph(self):

        self.dot_file = None
        if self.topology == 'From file' or 'From DOT file' in self.topology:
            self.dot_file = QFileDialog.getOpenFileName(self, 'Open file', './')

        target_dir = self.targets_folder()
        pe_lib = pe_library(target_dir)
        self.ng = NetGen(pe_lib)
        self.ng.build_graph(self.topology, self.bus_width, self.num_pes, file=self.dot_file,
                            avg_degree=self.avg_degree, std_dev=self.std_dev,
                            mesh_width=self.mesh_width, mesh_height=self.mesh_height,
                            dsp_per_pe=self.dsp_per_pe, ram_per_pe=self.ram_per_pe)

        if self.topology == 'Mesh':
            self.pe_textedit.setText(str(self.mesh_height * self.mesh_width))

        self.avg_outdeg_lineedit.setText('%.02f' % self.ng.get_avg_degree())
        self.total_nets.setText(str(self.ng.get_total_nets()))

    def generate_dot(self):
        self._gather_fields()
        self._set_fields()

        self._build_graph()

        output = self.ng.gen_output(lang='dot')

        self._get_dir()
        self._save_file(output, 'graph.txt')

    def generate_vhdl(self):
        self._gather_fields()
        self._set_fields()
        self._build_graph()

        output = self.ng.gen_output(lang='vhdl')

        self._get_dir()
        self._save_file(output, 'top.vhd')
        self._export_targets()

    def generate_constrained_vhdl(self):
        self._gather_fields()
        self._set_fields()
        self._build_graph()

        self._get_dir()
        if not self.directory: return

        vhdl = self.ng.gen_output(lang='vhdl')
        dot = self.ng.gen_output(lang='dot')
        self._save_file(vhdl, 'top.vhd')
        self._save_file(dot, 'graph.txt')

        if self.topology != 'From DOT file (pos-locked)':
            dot_struct = dot_parser.parse_dot_data(dot)
        else:
            with open(self.dot_file, 'r') as f:
                data = f.read()
            dot_struct = dot_parser.parse_dot_data(data)

        dot_graph = dot_struct.create_dot(prog='neato')

        dot_struct = dot_parser.parse_dot_data(dot_graph)

        # Dear God...
        # Don't bother trying to understand this :p.
        positions = {pe: coords for pe, coords in enumerate([n.get('pos').split(',') for n in [i for i in sorted([j for j in dot_struct.get_nodes() if 'pos' in j.obj_dict['attributes']], key=get_name) if 'pos' in i.obj_dict['attributes']]])}
        target = Target(os.path.join('targets', self.target, 'grids', self.grid))
        pe_cell_map, cell_pe_map = initial_placement(positions, target)
        self._save_file('\n'.join(['%d:%d' % (k, v[0]) for k, v in cell_pe_map.items()]), 'init_placement.txt')

        grid_sa = CDLL('GridSADLL.dll')
        config_file = os.path.join(sys.path[0], 'targets', self.target, 'grids', self.grid)
        assert(os.path.exists(config_file))
        assert(os.path.exists(self.directory))

        print '***Creating UCF from DOT placement***'

        try:
            grid_sa.RunDotPlacement(config_file, self.directory)
        except WindowsError as e:
            print 'Could not load GridSADLL.dll', e
            return

        print '***Creating UCF from Random placement***'

        try:
            grid_sa.RunRandomPlacement(config_file, self.directory)
        except WindowsError as e:
            print 'Could not load GridSADLL.dll', e
            return

        self._add_pblocks_to_ucf(target, os.path.join(self.directory, 'dot.ucf'))
        self._add_pblocks_to_ucf(target, os.path.join(self.directory, 'random.ucf'))
        self._export_targets()

    def _add_pblocks_to_ucf(self, target, ucf):
        with open(ucf, 'a') as f:
            f.write(target.config_tokens['pblocks'])

    def _export_targets(self):

        hdl_folder = os.path.join(self.directory, 'PEcores')

        target_folder = self.targets_folder()
        try:
            shutil.copytree(target_folder, hdl_folder)
        except WindowsError:
            print 'PECores already exists... Skipping'

        # Export pePool.vhd package also
        package = os.path.join(sys.path[0], 'targets', 'pePool.vhd')
        try:
            shutil.copy(package, hdl_folder)
        except WindowsError:
            print 'pePool.vhd already exists... Skipping'

    def _get_dir(self):
        self.directory = str(QFileDialog.getExistingDirectory(self, 'Select directory to save to'))

    def _save_file(self, output, filename):
        full_path = os.path.join(self.directory, filename)
        with open(full_path, 'w') as f:
            f.write(output)

    def targets_folder(self):
        return os.path.join(sys.path[0], 'targets', self.target, 'PEcores')

def get_name(item):
    return int(item.get_name())


if __name__ == "__main__":
    app = QApplication(sys.argv)

    window = GUI()
    window.show()

    sys.exit(app.exec_())
