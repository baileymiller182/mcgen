
library IEEE;
use IEEE.STD_LOGIC_1164.all;

package pePool is


component pe_1in_1dsp_2mem is
port(
   clk: in std_logic;
   inst_no: in std_logic_vector(9 downto 0);
   d1: in std_logic_vector(31 downto 0);
   dout: out std_logic_vector(31 downto 0)
);
end component;

component pe_1in_1dsp_1mem is
port(
   clk: in std_logic;
   inst_no: in std_logic_vector(9 downto 0);
   d1: in std_logic_vector(31 downto 0);
   dout: out std_logic_vector(31 downto 0)
);
end component;

component pe_1in_2dsp_0mem is
port(
   clk: in std_logic;
   inst_no: in std_logic_vector(9 downto 0);
   d1: in std_logic_vector(31 downto 0);
   dout: out std_logic_vector(31 downto 0)
);
end component;


component pe_1in_5dsp_0mem is
port(
   clk: in std_logic;
   inst_no: in std_logic_vector(9 downto 0);
   d1: in std_logic_vector(31 downto 0);
   dout: out std_logic_vector(31 downto 0)
);
end component;

component pe_1in_0dsp_2mem is
port(
   clk: in std_logic;
   inst_no: in std_logic_vector(9 downto 0);
   d1: in std_logic_vector(31 downto 0);
   dout: out std_logic_vector(31 downto 0)
);
end component;


component pe_3in_1dsp_2mem is
port(
   clk: in std_logic;
   inst_no: in std_logic_vector(9 downto 0);
   d1, d2, d3: in std_logic_vector(31 downto 0);
   dout: out std_logic_vector(31 downto 0)
);
end component;

component pe_3in_1dsp_1mem is
port(
   clk: in std_logic;
   inst_no: in std_logic_vector(9 downto 0);
   d1, d2, d3: in std_logic_vector(31 downto 0);
   dout: out std_logic_vector(31 downto 0)
);
end component;

component pe_3in_2dsp_0mem is
port(
   clk: in std_logic;
   inst_no: in std_logic_vector(9 downto 0);
   d1, d2, d3: in std_logic_vector(31 downto 0);
   dout: out std_logic_vector(31 downto 0)
);
end component;


component pe_3in_5dsp_0mem is
port(
   clk: in std_logic;
   inst_no: in std_logic_vector(9 downto 0);
   d1, d2, d3: in std_logic_vector(31 downto 0);
   dout: out std_logic_vector(31 downto 0)
);
end component;

component pe_3in_0dsp_2mem is
port(
   clk: in std_logic;
   inst_no: in std_logic_vector(9 downto 0);
   d1, d2, d3: in std_logic_vector(31 downto 0);
   dout: out std_logic_vector(31 downto 0)
);
end component;



component pe_7in_1dsp_2mem is
port(
   clk: in std_logic;
   inst_no: in std_logic_vector(9 downto 0);
   d1, d2, d3, d4, d5, d6, d7: in std_logic_vector(31 downto 0);
   dout: out std_logic_vector(31 downto 0)
);
end component;

component pe_7in_1dsp_1mem is
port(
   clk: in std_logic;
   inst_no: in std_logic_vector(9 downto 0);
   d1, d2, d3, d4, d5, d6, d7: in std_logic_vector(31 downto 0);
   dout: out std_logic_vector(31 downto 0)
);
end component;

component pe_7in_2dsp_0mem is
port(
   clk: in std_logic;
   inst_no: in std_logic_vector(9 downto 0);
   d1, d2, d3, d4, d5, d6, d7: in std_logic_vector(31 downto 0);
   dout: out std_logic_vector(31 downto 0)
);
end component;


component pe_7in_5dsp_0mem is
port(
   clk: in std_logic;
   inst_no: in std_logic_vector(9 downto 0);
   d1, d2, d3, d4, d5, d6, d7: in std_logic_vector(31 downto 0);
   dout: out std_logic_vector(31 downto 0)
);
end component;

component pe_7in_0dsp_2mem is
port(
   clk: in std_logic;
   inst_no: in std_logic_vector(9 downto 0);
   d1, d2, d3, d4, d5, d6, d7: in std_logic_vector(31 downto 0);
   dout: out std_logic_vector(31 downto 0)
);
end component;



component pe_15in_1dsp_2mem is
port(
   clk: in std_logic;
   inst_no: in std_logic_vector(9 downto 0);
   d1, d2, d3, d4, d5, d6, d7, d8, d9, d10, d11, d12, d13, d14, d15: in std_logic_vector(31 downto 0);
   dout: out std_logic_vector(31 downto 0)
);
end component;

component pe_15in_1dsp_1mem is
port(
   clk: in std_logic;
   inst_no: in std_logic_vector(9 downto 0);
   d1, d2, d3, d4, d5, d6, d7, d8, d9, d10, d11, d12, d13, d14, d15: in std_logic_vector(31 downto 0);
   dout: out std_logic_vector(31 downto 0)
);
end component;

component pe_15in_2dsp_0mem is
port(
   clk: in std_logic;
   inst_no: in std_logic_vector(9 downto 0);
   d1, d2, d3, d4, d5, d6, d7, d8, d9, d10, d11, d12, d13, d14, d15: in std_logic_vector(31 downto 0);
   dout: out std_logic_vector(31 downto 0)
);
end component;

component pe_15in_5dsp_0mem is
port(
   clk: in std_logic;
   inst_no: in std_logic_vector(9 downto 0);
   d1, d2, d3, d4, d5, d6, d7, d8, d9, d10, d11, d12, d13, d14, d15: in std_logic_vector(31 downto 0);
   dout: out std_logic_vector(31 downto 0)
);
end component;

component pe_15in_0dsp_2mem is
port(
   clk: in std_logic;
   inst_no: in std_logic_vector(9 downto 0);
   d1, d2, d3, d4, d5, d6, d7, d8, d9, d10, d11, d12, d13, d14, d15: in std_logic_vector(31 downto 0);
   dout: out std_logic_vector(31 downto 0)
);
end component;

end pePool;

