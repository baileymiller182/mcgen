__author__ = 'bmiller'

import random
import re
from generators.gen_vhdl import gen_vhdl
from generators.gen_dot import gen_dot
from collections import defaultdict

from pe_lib import pe_type


class NetGen(object):
    def __init__(self, pe_lib):
        self.graph = {}
        self.pe_types = {}
        self.pe_lib = pe_lib

    def build_graph(self, topology='Custom 1', bus_width='32', num_pes='128',
                    file=None, avg_degree=None, std_dev=None, mesh_width=None,
                    mesh_height=None, dsp_per_pe=1, ram_per_pe=1):
        if file:
            if topology == 'From file':
                self._build_from_file(str(file))
            elif topology == 'From DOT file' or topology == 'From DOT file (pos-locked)':
                self._build_from_DOT_file(str(file))

        else:
            if topology == 'Strongly connected':
                self._build_strongly_connected(num_pes)
            elif topology == 'Random':
                self._build_random(num_pes, avg_degree, std_dev)
            elif topology == 'Ring':
                self._build_ring(num_pes)
            elif topology == 'Tree':
                self._build_tree(num_pes)
            elif topology == 'Mesh':
                self._build_mesh(mesh_height, mesh_width)

        self._assign_homogeneous_types(dsp_per_pe, ram_per_pe)

    def gen_output(self, lang='vhdl'):
        output = ''
        if lang == 'vhdl':
            output = gen_vhdl(self.graph, self.pe_types)
        elif lang == 'dot':
            #TODO: add types to DOT gen
            output = gen_dot(self.graph)

        return output

    def _assign_homogeneous_types(self, dsps, rams):
        #Find number of inputs for each PE
        pe_inputs = defaultdict(int)
        for source, destinations in self.graph.items():
            for dest in destinations:
                pe_inputs[dest] += 1

        for pe in self.graph:
            if not self.pe_lib.pe_valid(pe_inputs[pe], dsps, rams):
                raise BaseException('PE does not exist')
            self.pe_types[pe] = pe_type(pe_inputs[pe], dsps, rams)

    def print_graph(self):
        print self.graph

    def _build_from_file(self, file):
        '''
        Builds a network from a pre-created file.
         File has the syntax:
           source: from1, from2, etc...

           For example:
           0: 1, 2
           1: 0, 3
           ...
        '''
        with open(file, 'r') as f:
            for line in f:
                pe, connections = line.split(':')
                self.graph[int(pe)] = [int(s.strip()) for s in connections.split(",") if len(s.strip()) > 0]

    def _build_from_DOT_file(self, file):
        pattern = re.compile(' *(\d+) -> (\d+)')
        with open(file, 'r') as f:
            for line in f:
                result = pattern.match(line)
                if result:
                    source = int(result.groups()[0])
                    dest = int(result.groups()[1])
                    if source in self.graph:
                        self.graph[source] += [dest]
                    else:
                        self.graph[source] = [dest]

        # Reduce high node IDs to < num_pes
        num_pes = len(self.graph)
        used = [i for i in self.graph if i < num_pes]
        new_graph = {i: j for i, j in self.graph.items() if i in used}
        remap = {}

        for pe in self.graph:
            if pe >= num_pes:
                unused = next(i for i in xrange(num_pes) if i not in used)
                used.append(unused)
                new_graph[unused] = self.graph[pe]
                remap[pe] = unused

        for src, dests in new_graph.items():
            for d in dests[:]:
                if d in remap:
                    dests.remove(d)
                    dests.append(remap[d])

        self.graph.update(new_graph)

    def _build_strongly_connected(self, num_pes):
        for pe in range(num_pes):
            self.graph[pe] = []
            [self.graph[pe].append(n) for n in range(num_pes) if pe != n]

    def _build_random(self, num_pes, avg, std_dev):
        random.seed()
        for pe in range(num_pes):
            num_connections = int(random.gauss(avg, std_dev))
            num_connections = num_connections if num_connections > 0 else 1
            self.graph[pe] = []
            while len(self.graph[pe]) < num_connections:
                destination = pe
                while destination == pe or destination in self.graph[pe]:
                    destination = int(random.uniform(0, num_pes - 1))
                self.graph[pe].append(destination)

        # Ensure at least 1 input per PE
        pe_inputs = {}
        for i in range(num_pes):
            pe_inputs[i] = []
        for source, destinations in self.graph.items():
            for dest in destinations:
                pe_inputs[dest].append(source)

        for pe in pe_inputs:
            if len(pe_inputs[pe]) == 0:
                source = pe
                while source == pe:
                    source = int(random.uniform(0, num_pes-1))
                self.graph[source].append(pe)

    def _build_ring(self, num_pes):
        for pe in range(num_pes)[1:-1]:
            self.graph[pe] = [pe-1, pe+1]
        self.graph[0] = [num_pes-1, 1]
        self.graph[num_pes-1] = [num_pes-2, 0]

    def _build_tree(self, num_pes):
        for pe in range(num_pes):
            self.graph[pe] = []

        for pe in range(num_pes/2):
            if num_pes-1 < pe*2+1: break
            elif num_pes-1 < pe*2+2:
                self.graph[pe].append(pe*2 + 1)
                self.graph[pe*2 + 1].append(pe)
            else:
                self.graph[pe].append(pe*2 + 1)
                self.graph[pe].append(pe*2 + 2)
                self.graph[pe*2 + 1].append(pe)
                self.graph[pe*2 + 2].append(pe)

    def _build_mesh(self, mesh_width, mesh_height):
        for row in range(mesh_height):
            for col in range(mesh_width):
                pe = row*mesh_width+col
                self.graph[pe] = []
                if 0 <= row < mesh_height-1:
                    self.graph[pe].append(pe+mesh_width)  # Connect to below PE
                if 0 < row < mesh_height:
                    self.graph[pe].append(pe-mesh_width)  # Connect to upper PE
                if (col%mesh_width) > 0:
                    self.graph[pe].append(pe-1)  # Connect to left PE
                if (col%mesh_width) < mesh_width-1:
                    self.graph[pe].append(pe+1)  # Connect to right PE

    def get_avg_degree(self):
        return sum([len(i) for i in self.graph.values()])/float(len(self.graph))

    def get_total_nets(self):
        return sum(sum(i) for i in self.graph.values()) * 32