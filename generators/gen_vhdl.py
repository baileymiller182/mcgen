__author__ = 'bmiller'

from collections import defaultdict

from pe_lib import input_ceil

header = ('library IEEE;\n'
          'use IEEE.STD_LOGIC_1164.all;\n'
          'use work.pePool.all;\n'
          '\nentity top is\n'
          'port(\n'
          '  clk: in std_logic;\n'
          '  inst_no: in std_logic_vector(9 downto 0);\n'
          '  dout: out std_logic_vector(31 downto 0)\n);\n'
          'end top;\n\n'
          'architecture Behavioral of top is\n')




def gen_signals(graph):
    signals = ''
    for pe in graph:
        signals += 'signal pe%d_dout: std_logic_vector(31 downto 0);\n' % pe

    signals += 'constant zero: std_logic_vector(31 downto 0) := X"00000000";\n'

    return signals


def gen_vhdl(graph, types):
    vhdl = header[:]

    vhdl += gen_signals(graph)

    vhdl += '\nbegin\n'

    pe_inputs = defaultdict(list)
    for source, destinations in graph.items():
        for dest in destinations:
            pe_inputs[dest].append(source)

    for pe in graph:
        vhdl += '\npe%(num)d: pe_%(inputs)din_%(dsp)ddsp_%(bram)dmem\n' % \
                {
                    'num': pe,
                    'inputs': input_ceil(types[pe].inputs),
                    'dsp': types[pe].dsp,
                    'bram': types[pe].ram
                }
        vhdl += 'port map(clk, inst_no, '
        for conn in pe_inputs[pe]:
            vhdl += 'pe%d_dout, ' % conn

        vhdl += 'zero, ' * (input_ceil(len(pe_inputs[pe])) - len(pe_inputs[pe]))
        vhdl += 'pe%d_dout);\n' % pe

    vhdl += '\ndout <= peFIXME_dout;\n'
    vhdl += 'end Behavioral;\n'

    return vhdl
