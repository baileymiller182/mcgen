__author__ = 'bmiller'

def gen_dot(graph):
    dot = 'digraph g {\n  node [shape=circle]; '

    dot += ' '.join([str(i) for i in graph]) + ';\n'

    for pe1 in graph:
        for pe2 in graph[pe1]:
            dot += '  %d -> %d;\n' % (pe1, pe2)

    dot += '}'

    return dot