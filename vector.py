__author__ = 'bmiller'

import math

class Vector(object):
    def __init__(self, magnitude=0.0, theta=0.0):
        self.magnitude = magnitude
        self.theta = theta

    def get_x(self):
        return self.magnitude * math.cos(self.theta)

    def get_y(self):
        return self.magnitude * math.sin(self.theta)

    def __add__(self, other):
        t = Vector()
        t.magnitude = math.sqrt((self.get_x() + other.get_x())**2 + (self.get_y() + other.get_y())**2)
        t.theta = math.atan((self.get_y() + other.get_y()) / (self.get_x() + other.get_x()))
        return t
