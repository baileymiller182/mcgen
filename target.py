__author__ = 'bmiller'

'''
Describes a target platform grid
'''


class Target(object):
    def __init__(self, config):
        self.config_tokens = {}
        self._load_config(config)
        if 'ignore' in self.config_tokens:
            self.ignored = [int(i.strip()) for i in self.config_tokens['ignore'].split(',')]
        else:
            self.ignored = []

    def _load_config(self, path):
        with open(path, 'r') as f:
            for line in f:
                arg, val = line.split('=')

                # pblocks must be last config item
                if arg.lower() != 'pblocks':
                    self.config_tokens[arg] = self._try_type_cast(val)
                else:
                    break

            self.config_tokens['pblocks'] = '\n'.join([i for i in f])

    def _try_type_cast(self, item):
        try:
            return float(item)
        except ValueError:
            pass

        try:
            return int(item)
        except ValueError:
            pass

        return item

    def is_ignored(self, item):
        return item in self.ignored

    def __getattr__(self, item):
        return self.config_tokens[item]
