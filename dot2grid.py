__author__ = 'bmiller'

import copy
import math

from vector import Vector

global _target

def initial_placement(nodes, target):
    """
    Convert dot placements to a (valid) grid
    """
    global _target
    _target = target

    nodes = copy.deepcopy(nodes)  # No side effects...

    #Convert crappy string into floats
    for k, v in nodes.items():
        newv = []
        for i in v:
            newv.append(float(i.replace('"', '')))
        nodes[k] = newv

    # Scale node positions to graph
    x_coords, y_coords = [list(i) for i in zip(*nodes.values())]

    if min(x_coords) > 0:
        nodes = {node: (coords[0] - min(x_coords), coords[1]) for node, coords, in nodes.items()}
    else:
        nodes = {node: (coords[0] + min(x_coords), coords[1]) for node, coords, in nodes.items()}

    if min(y_coords) > 0:
        nodes = {node: (coords[0], coords[1] - min(y_coords)) for node, coords, in nodes.items()}
    else:
        nodes = {node: (coords[0], coords[1] + min(y_coords)) for node, coords, in nodes.items()}

    pixels_per_col = (max(x_coords) - min(x_coords)) / (target.grid_width - 1)
    pixels_per_row = (max(y_coords) - min(y_coords)) / (target.grid_height - 1)

    if pixels_per_row == 0:
        pixels_per_row = 1
    if pixels_per_col == 0:
        pixels_per_col = 1

    grid = {}
    for pe, coords in nodes.items():
        col = int(coords[0] // pixels_per_col)
        row = int(coords[1] // pixels_per_row)
        loc = int(row*target.grid_width + col)
        grid[pe] = loc


    # We now have a potentially invalid grid placement.

    # Now let's force it to be valid by enforcing:
        # 1. Must be 1 PE per grid cell
        # 2. Must fit within row/columns of grid (already done?)
        # 3. Not ignored by target (i.e., architectural gap)
        # 4. Profit

    # Maps each physical loc to assigned PEs
    cell_map = {}

    for pe, cell in grid.items():
        if cell not in cell_map:
            cell_map[cell] = [pe]
        else:
            cell_map[cell].append(pe)

    print_grid(cell_map)
    print '\n*********Above is before forcing valid placement********\n'
    print '\n*********Below is after forcing valid placement********\n'

    # Ensure each physical loc only contains 1 PE
    # and that the loc is present on target (not ignored)
    for cell, pes in cell_map.items():
        if _target.is_ignored(cell):
            for pe in pes[:]:
                _move_pe(cell, pe, cell_map)
            del cell_map[cell]
        elif len(pes) > 1:
            for pe in pes[1:]:
                _move_pe(cell, pe, cell_map)

    #Update pe->cell map
    grid.clear()
    for cell, pe in cell_map.items():
        grid[pe[0]] = cell

    for cell in range(int(_target.grid_width*_target.grid_height)):
        if cell not in cell_map:
            cell_map[cell] = [-1]  # Mark cell not used

    print_grid(cell_map)

    return grid, cell_map


def print_grid(cell_map):
    for row in reversed(range(int(_target.grid_height))):
        for col in range(int(_target.grid_width)):
            cell = row*int(_target.grid_width) + col
            if cell in cell_map:
                print '%03d' % cell_map[cell][0],
            else:
                print '%03s' % ' ',
        print


# Move a PE to the closest open cell.
# Searches in an increasing radius from the original cell
def _move_pe(cell, pe, cell_map):
    radius = 1
    while pe in cell_map[cell]:
        neighbors = get_neighbors(radius, cell)
        try:
            empty_cell = next((i for i in neighbors if (i not in cell_map) or (len(cell_map[i]) == 0)))
        except StopIteration:
            radius += 1
            continue
        else:
            cell_map[empty_cell] = [pe]
            cell_map[cell].remove(pe)
            # print 'forced %d from loc %d to loc %d' % (pe, cell, empty_cell)
            break


def get_neighbors(radius, cell):
    neighbors = set()
    diff_theta = radius_scan_map(radius)

    curr_theta = 0
    while curr_theta < 2*math.pi:
        vec = Vector(radius, curr_theta)

        newx = int(math.ceil(cell_x(cell) + vec.get_x()))
        newy = int(math.ceil(cell_y(cell) + vec.get_y()))

        if (0 > newx) or (newx >= _target.grid_width) or (0 > newy) or (newx >= _target.grid_height):
            curr_theta += diff_theta
            continue

        new_loc = int(newy*_target.grid_width + newx)

        if new_loc >= _target.grid_width * _target.grid_height:
            curr_theta += diff_theta
            continue

        if _target.is_ignored(new_loc):
            curr_theta += diff_theta
            continue

        neighbors.add(new_loc)

        curr_theta += diff_theta

    return neighbors

def cell_x(cell):
    return int(cell % _target.grid_width)


def cell_y(cell):
    return int(cell / _target.grid_width)


def radius_scan_map(radius):
    '''
    Guarantees to hit all neighbors at a specified radius from center in 2D mesh
    '''
    return (math.pi/4.0) / radius

