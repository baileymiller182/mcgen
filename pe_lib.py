__author__ = 'bmiller'

import os
import re
from collections import namedtuple

pe_type = namedtuple('pe_type', ['inputs', 'dsp', 'ram'])


def input_ceil(num):
    if num == 1:
        return 1
    elif num <= 3:
        return 3
    elif num <= 7:
        return 7
    elif num <= 15:
        return 15
    elif num <= 31:
        return 31
    else:
        raise('Too many connections: %d' % num)


class pe_library(object):
    def __init__(self, target_dir):
        self.cores = {}
        self._load_cores(target_dir)

    def _load_cores(self, dir):
        name_re = re.compile(r"pe_(\d+)in_(\d+)dsp_(\d+)mem")
        for file in os.listdir(dir):
            if file.endswith('.ngc') or file.endswith('.vhd'):
                result = re.match(name_re, file)
                if result:
                    inputs, dsp, ram = [int(x) for x in result.groups()]
                    self.cores[pe_type(inputs, dsp, ram)] = file

    def pe_valid(self, inputs, dsp, ram):
        if not (input_ceil(inputs), dsp, ram) in self.cores:
            print 'No PE Core %d inputs, %d dsp, %d ram available' % (inputs, dsp, ram)
            return False
        return True
